<?php
/**
 * @file
 * Menu callback functions and other routine functions
 */

/**
 * Gets interswitch configuration from DB.
 *
 * @return array
 *   An associative array of configuration or an empty array if no configuration
 *   is found.
 */
function _interswitch_donate_get_config() {
  $base_url = str_replace('www.', '',
    parse_url(url('', array('absolute' => TRUE)), PHP_URL_HOST));
  $c = array();

  $r = db_query("SELECT * FROM {interswitch_donate_config}");
  $rec = db_fetch_object($r);
  if ($rec) {
    $c['donation_areas'] = '';
    foreach (explode(PHP_EOL, $rec->donation_areas) as $d) {
      if ($c['donation_areas']) {
        $c['donation_areas'] .= PHP_EOL . $d;
      }
      else {
        $c['donation_areas'] = $d;
      }
    }
    $c['mac_key'] = $rec->mac_key;
    $c['product_id'] = $rec->product_id;
    $c['pay_item_id'] = $rec->pay_item_id;
    $c['notification_email'] = $rec->notification_email . "@$base_url";
    $c['server'] = $rec->server;
  }

  return $c;
}

/**
 * Creates a transaction ID for donation and inserts the details in the DB.
 *
 * Transaction ID schema is userid-amount-timestamp
 *
 * @param float $amount
 *   Amount for donationn
 * @param string $dp
 *   The purpose of the donation
 *
 * @return string
 *   Transaction ID if successful. False otherwise
 */
function _interswitch_donate_create_txn_id($amount, $dp) {
  if (!$amount || !$dp) {
    return FALSE;
  }

  global $user;
  $time = time();
  $txn_id = $user->uid . '-' . $amount . '-' . $time;
  $args = array($txn_id, $user->uid, $time, $amount, $dp);
  $q = "INSERT INTO {interswitch_donate_donations} "
    . "(txn_id, user_id, created, amount, purpose) "
    . "VALUES ('%s', %d, %d, %d, '%s')";

  if (!db_query($q, $args)) {
    return FALSE;
  }

  return $txn_id;
}

/**
 * Page callback for admin/settings/interswitch-donate.
 *
 * See http://drupal.org/node/751826 for Drupal's Form API
 */
function _interswitch_donate_settings_form($form_state) {
  // Get current merchant id and cadpid.
  // Only 1 or 0 row should be in the DB.
  $c = _interswitch_donate_get_config();

  $s = drupal_get_schema('interswitch_donate_config');
  $form = array();

  $form['mac_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter MAC Key'),
      '#required' => TRUE,
      '#default_value' => $c['mac_key'],
      '#maxlength' => $s['fields']['mac_key']['length']
  );
  $form['product_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter product ID'),
      '#required' => TRUE,
      '#default_value' => $c['product_id'],
      '#maxlength' => $s['fields']['product_id']['length']
  );
  $form['pay_item_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter pay item ID'),
      '#required' => TRUE,
      '#default_value' => $c['pay_item_id'],
      '#maxlength' => $s['fields']['pay_item_id']['length']
  );
  $base_url = str_replace('www.', '',
      parse_url(url('', array('absolute' => TRUE)), PHP_URL_HOST));
  $form['notification_email'] = array(
      '#type' => 'textfield',
      '#title' => t('Notification email'),
      '#required' => TRUE,
      '#default_value' => str_replace("@$base_url", '', $c['notification_email']),
      '#field_suffix' => '@' . $base_url,
      '#maxlength' => $s['fields']['notification_email']['length']
  );
  $form['server'] = array(
      '#type' => 'select',
      '#title' => 'Interswitch Server',
      '#default_value' => $c['server'],
      '#options' => array(
          'demo' => 'Demo server - ' . INTERSWITCH_DONATE_DEMO_SERVER,
          'live' => 'Live server - ' . INTERSWITCH_DONATE_LIVE_SERVER
      )
  );
  $form['donation_areas'] = array(
      '#type' => 'textarea',
      '#title' => t('Areas of donation'),
      '#default_value' => $c['donation_areas'],
      '#required' => TRUE,
      '#description' => t('Specify one area of donation per line')
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update Credentials')
  );

  return $form;
}

/**
 * Validation callback for _interswitch_donate_settings_form().
 *
 * See http://drupal.org/node/751826 for Drupal's Form API
 */
function _interswitch_donate_settings_form_validate($form_id, $form_state) {
  $base_url = str_replace('www.', '',
      parse_url(url('', array('absolute' => TRUE)), PHP_URL_HOST));
  $email = trim($form_state['values']['notification_email']) . '@' . $base_url;
  $s = drupal_get_schema('interswitch_donate_config');
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    form_set_error('notification_email',
    t('The email address appears to be invalid'));
  }
  elseif (strlen($email) > $s['fields']['notification_email']['length']) {
    form_set_error('notification_email',
      t('The email address should not be longer than !length characters.',
        array('!length' => $s['fields']['notification_email']['length'])
      )
    );
  }

  // Ensure that there are not duplicates in the donation areas.
  $da = explode(PHP_EOL, $form_state['values']['donation_areas']);
  $da2 = explode(PHP_EOL, $form_state['values']['donation_areas']);
  sort($da2);
  $duplicates = '';
  foreach ($da2 as $k => $v) {
    if (count(array_keys($da, $v, TRUE)) > 1) {
      if (!$duplicates) {
        $duplicates = '<br/>' . $v;
      }
      else {
        $duplicates .= '<br/>' . $v;
      }
    }
  }
  if ($duplicates) {
    form_set_error('donation_areas', t('The following duplicates were found: ')
    . $duplicates);
  }
}

/**
 * Submission callback for _interswitch_donate_settings_form().
 *
 * See http://drupal.org/node/751826 for Drupal's Form API
 */
function _interswitch_donate_settings_form_submit($form_id, &$form_state) {
  // Clear existings credentials.
  db_query("DELETE FROM {interswitch_donate_config}");

  // Insert new credentials.
  // Start by making all donation areas Proper Case.
  $arr = explode(';', $form_state['values']['donation_areas']);
  $donation_areas = '';
  for ($i = 0; $i < count($arr); ++$i) {
    if (0 == $i) {
      $donation_areas = ucwords(strtolower(trim($arr[$i])));
    }
    else {
      $donation_areas .= PHP_EOL . ucwords(strtolower(trim($arr[$i])));
    }

  }

  $data = array(
      'mac_key' => $form_state['values']['mac_key'],
      'product_id' => $form_state['values']['product_id'],
      'pay_item_id' => $form_state['values']['pay_item_id'],
      'notification_email' => trim($form_state['values']['notification_email']),
      'donation_areas' => $donation_areas,
      'server' => $form_state['values']['server']
  );

  if (SAVED_NEW == drupal_write_record('interswitch_donate_config', $data)) {
    drupal_set_message(t('Credentials successfully updated.'), 'status');
  }
  else {
    drupal_set_message(t('Error Credentials successfully updated.'), 'error');
  }
}

/**
 * Displays donations for current user in a table.
 *
 * If the current user is admin then it displays the donations for all users.
 * The donations can be filtered by status and time.
 *
 * @param bool $view_all_users
 *   If true displays the history for all users with some extra fields. This
 *   should be for admin or user with permissions to view all history.
 *   If false displays the history for the current user only.
 * @param string $status
 *   Status of payment: 'completed', 'pending' or 'cancelled'
 * @param int $start_time
 *   Timestamp of payment start time
 * @param int $end_time
 *   Timestamp of payment end time
 *
 * @return string
 *   Donations in an HTML table
 */
function _interswitch_donate_donation_history($view_all_users = FALSE, $status = 'all', $start_time = 0, $end_time = 0) {
  // Fix up all user-submitted variables.
  $end_time = (int) $end_time;
  if (0 == $end_time) {
    $end_time = time();
  }
  $start_time = (int) $start_time;
  $status = strtolower($status);
  if ($status != 'all' && $status != 'completed' && $status != 'pending'
      && $status != 'cancelled') {
    $status = 'all';
  }

  drupal_add_js(
    drupal_get_path('module', 'interswitch_donate')
    . '/interswitch_donate.js'
  );

  drupal_add_css(
  drupal_get_path('module', 'interswitch_donate')
  . '/interswitch_donate.css',
  'module', 'all', FALSE
  );

  global $user;
  date_default_timezone_set('Africa/Lagos');
  // Create header.
  $header = array(
      array(
        'data' => t('Transaction ID'),
        'field' => 'txn_id'
      ),
      array(
        'data' => t('Status'),
        'field' => 'status'
      ),
      array(
        'data' => t('Purpose'),
        'field' => 'purpose'
      ),
      array(
        'data' => t('Start Time'),
        'field' => 'created',
        'sort' => 'desc'
      ),
      array(
        'data' => t('End Time'),
        'field' => 'completed'
      ),
      array(
        'data' => t('Payment Successful'),
      ),
      array(
        'data' => t('Donation Amount (!symbol)', array('!symbol' => '&#8358;')),
        'field' => 'amount'
      )
  );

  $sql_args = array();
  $q = '';
  $q_successful = '';
  $q_not_successful = '';

  if ($view_all_users) {
    // If all user history is to be displayed add extra fields.
    $header[] = array(
      'data' => t('User ID'),
      'field' => 'user_id'
    );
    $header[] = array(
      'data' => t('Response Code'),
      'field' => 'response_code'
    );
    $header[] = array(
      'data' => t('Response Description'),
      'field' => 'response_description'
    );
    $header[] = array('data' => t('Action'));

    // Use a query that corresponds with the fields.
    $q = 'SELECT * FROM {interswitch_donate_donations}'
        . " WHERE created >= %d AND created <= %d";

    $q_successful = 'SELECT SUM(amount) FROM {interswitch_donate_donations}'
        . ' WHERE created >= %d AND created <= %d'
            . " AND response_code = '00'";

    $q_not_successful = 'SELECT SUM(amount) FROM {interswitch_donate_donations}'
        . ' WHERE created >= %d AND created <= %d'
            . " AND response_code <> '00'";

    $sql_args[] = $start_time;
    $sql_args[] = $end_time;
  }
  else {
    $q = 'SELECT txn_id, status, purpose, created, completed, amount,'
        . ' amount_paid, response_code FROM {interswitch_donate_donations}'
        . ' WHERE user_id = %d AND created >= %d AND created <= %d';

    $q_successful = 'SELECT SUM(amount) FROM {interswitch_donate_donations}'
        . ' WHERE user_id = %d AND created >= %d'
            . " AND created <= %d AND response_code = '00'";

    $q_not_successful = 'SELECT SUM(amount) FROM {interswitch_donate_donations}'
        . ' WHERE user_id = %d AND created >= %d'
        . " AND created <= %d AND response_code <> '00'";

    $sql_args[] = $user->uid;
    $sql_args[] = $start_time;
    $sql_args[] = $end_time;
  }

  // Add status if applicable.
  if ($status != 'all') {
    $q .= " AND status = '%s'";
    $q_successful .= " AND status = '%s'";
    $q_not_successful .= " AND status = '%s'";
    $sql_args[] = $status;
  }

  // Add this otherwise the table cannot be sorted.
  $q .= tablesort_sql($header);

  // Records per page.
  $count = 50;
  $r = pager_query($q, $count, 0, NULL, $sql_args);

  $r_temp = db_query($q_successful, $sql_args);
  $temp = db_fetch_array($r_temp);
  $amount_successful = number_format($temp['SUM(amount)'], 2);

  $r_temp = db_query($q_not_successful, $sql_args);
  $temp = db_fetch_array($r_temp);
  $amount_not_successful = number_format($temp['SUM(amount)'], 2);

  $rec = db_fetch_object($r);
  if (!$rec) {
    return t('You have not made any donations<br/><a href = "@url">Make a donation now</a>',
      array('@url' => url('donate')));
  }

  // Body of table.
  $rows = array();
  $payment_successful = '';
  $completed = '';
  do {
    if ('00' == $rec->response_code) {
      $payment_successful = 'Yes';
    }
    else {
      $payment_successful = 'No';
    }
    if ($rec->completed) {
      $completed = date('d-M-Y g:i A', $rec->completed);
    }
    else {
      $completed = '';
    }

    if ($view_all_users) {
      $rows[] = array(
        $rec->txn_id,
        ucwords($rec->status),
        $rec->purpose,
        date('d-M-Y g:i A', $rec->created),
        $completed,
        $payment_successful,
        number_format($rec->amount, 2),
        l($rec->user_id, 'user/' . $rec->user_id),
        $rec->response_code,
        $rec->response_description,
        "<input type ='button' value = 'Re-query'/>",
      );
    }
    else {
      $rows[] = array(
        $rec->txn_id,
        ucwords($rec->status),
        $rec->purpose,
        date('d-M-Y g:i A', $rec->created),
        $completed,
        $payment_successful,
        number_format($rec->amount, 2)
      );
    }

    $rec = db_fetch_object($r);
  } while (FALSE != $rec);

  // Store the SQL used to generate the table (without the LIMIT clause). It
  // will be used for the CSV download.
  $sql_text = '';
  switch (count($sql_args)) {
    case 2:
      $sql_text = sprintf($q, $sql_args[0], $sql_args[1]);
      break;

    case 3:
      $sql_text = sprintf($q, $sql_args[0], $sql_args[1], $sql_args[2]);
      break;

    case 4:
      $sql_text = sprintf($q, $sql_args[0], $sql_args[1], $sql_args[2],
      $sql_args[3]);
      break;
  }
  $_SESSION['interswitch_donate_sql_text'] = $sql_text;

  // There will be an ajax script on this page. The script will need to know
  // the base url in order to work. So we're going to insert the script in a
  // hidden <input />
  global $base_url;
  $site_base_url = "<input type = 'hidden' "
      . "id = 'interswitch_donate_site_base_url' value = '$base_url'/>";

  // Build the filter form.
  $form = drupal_get_form('_interswitch_donate_filter_history');

  $payment_summary = t("Successful Payments: <strong>!symbol$amount_successful</strong><br/>Unsuccessful Payments: <strong>!symbol$amount_not_successful</strong>",
    array('!symbol' => '&#8358;')
  );

  // Build download form.
  $download_button = drupal_get_form('_interswitch_donate_download_history');

  return $payment_summary . '<br/>' . $form . $download_button
  . '<br/>' . theme('table', $header, $rows) . theme('pager', $count)
  . $site_base_url;
}

/**
 * Builds a form for filtering donation history by date and status.
 *
 * @return array
 *   form array filter form
 */
function _interswitch_donate_filter_history() {
  $form = array();

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['options']['status'] = array(
    '#type' => 'select',
    '#title' => 'Status',
    '#options' => array(
        'all' => t('All'),
        'completed' => t('Completed'),
        'cancelled' => t('Cancelled'),
        'pending' => t('Pending')
    ),
    '#default_value' => 'all',
    '#attributes' => array('class' => array('container-inline'))
  );

  $form['options']['start_date'] = array(
    '#type' => 'date_popup',
    '#title' => 'Start date',
    '#date_format' => 'd-M-Y',
    '#date_label_position' => 'above'
  );

  $form['options']['end_date'] = array(
    '#type' => 'date_popup',
    '#title' => 'End date',
    '#date_format' => 'd-M-Y',
    '#date_label_position' => 'above'
  );

  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Filter',
    '#attributes' => array('class' => 'container-inline')
  );

  return $form;
}

/**
 * Submit callback.
 */
function _interswitch_donate_filter_history_submit($form_id, &$form_state) {
  // This function can be called on the admin or user interface ie either
  // admin/settings/interswitch-donate/view-all-history or view-donations.
  // So we need to determine which it is and redirect appropriately.
  $url = '';
  if (FALSE === strstr($form_id['#action'], 'view-donations')) {
    $url = 'admin/settings/interswitch-donate/view-all-history';
  }
  else {
    $url = 'view-donations';
  }

  $start_date = (int) strtotime($form_state['values']['start_date']);
  $end_date = (int) strtotime($form_state['values']['end_date']);

  $form_state['redirect'] = array(
      $url . '/' . $form_state['values']['status'] . "/$start_date/$end_date"
  );
}

/**
 * Form callback that creates a download button on the history page.
 */
function _interswitch_donate_download_history() {
  $form['download_csv'] = array(
      '#type' => 'submit',
      '#value' => 'Download this table in CSV'
  );

  return $form;
}

/**
 * Submit callback.
 */
function _interswitch_donate_download_history_submit() {
  if (!$_SESSION['interswitch_donate_sql_text']) {
    return;
  }

  // Create the CSV for download.
  $r = db_query($_SESSION['interswitch_donate_sql_text']);
  unset($_SESSION['interswitch_donate_sql_text']);

  $rec = db_fetch_array($r);
  $hdr = '';
  $data = '';
  $loop_count = 0;
  while (FALSE != $rec) {
    foreach ($rec as $k => $v) {
      // Create header row.
      if (!$loop_count) {
        if ('created' == $k) {
          $k = 'Start Time';
        }
        if ('completed' == $k) {
          $k = 'End Time';
        }
        $hdr .= '"' . ucwords(str_replace('_', ' ', $k)) . '",';
      }

      if ('Start Time' == $k || 'created' == $k) {
        $v = date('d-M-Y g:i A', $v);
      }
      if ('End Time' == $k || 'completed' == $k) {
        if ($v) {
          $v = date('d-M-Y g:i A', $v);
        }
        else {
          $v = 'N/A';
        }
      }
      $data .= '"' . $v . '",';
    }
    $data .= "\n";
    ++$loop_count;

    $rec = db_fetch_array($r);
  }
  drupal_set_header("Content-type: file");
  drupal_set_header("Content-transfer-encoding: binary");
  drupal_set_header("Content-Disposition: attachment; filename = donations.csv");
  echo $hdr . "\n" . $data;
}

/**
 * Page callback for donate.
 *
 * See http://drupal.org/node/751826 for Drupal's Form API
 */
function _interswitch_donate_donate($form_state) {
  $c = _interswitch_donate_get_config();

  $form = array();

  // If credentials not set.
  if (!$c) {
    $error = '<p>'
      . t('The Interswitch settings have not been configured. Please goto to the <a href = "@settings-page">settings page</a> to configure them.',
          array('@settings-page' => url('admin/settings/interswitch-donate')))
          . '</p>';
    $form['error'] = array(
      '#value' => $error
    );

    return $form;
  }

  $donation_areas = array();
  foreach (explode(PHP_EOL, $c['donation_areas']) as $d) {
    $d = trim($d);
    $donation_areas[$d] = $d;
  }
  $form['about'] = array(
      '#type' => 'item',
      '#title' => t('Information'),
      '#value' => t("Make a donation using your Interswitch ATM bank card.<br/>Enter the amount you want to donate. Thereafter you will be taken to Interswitch's website to conclude the payment.")
  );
  $form['donation_purpose'] = array(
    '#type' => 'radios',
    '#title' => t('Select an endeavour/purpose you want to donate to'),
    '#options' => $donation_areas,
    '#required' => TRUE
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter donation amount'),
    '#required' => TRUE,
    '#default_value' => 0,
    '#maxlength' => 7,
    '#description' => t('Enter the amount without any symbols eg instead of N1,000 enter 1000')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Donate')
  );
  // Add Interswitch logo.
  $form['#suffix'] = "<img src = '"
    . drupal_get_path('module', 'interswitch_donate')
    . "/images/logos.png'/>";

  return $form;
}

/**
 * Validation callback for _interswitch_donate_donate().
 *
 * See http://drupal.org/node/751826 for Drupal's Form API
 */
function _interswitch_donate_donate_validate($form, &$form_state) {
  if ('' == $form_state['values']['donation_purpose']) {
    return;
  }
  if (!preg_match('/^\d+(\.\d{1,2})?$/', $form_state['values']['amount'])) {
    form_set_error('amount', t('The value entered for amount is invalid'));
    return;
  }
  elseif ($form_state['values']['amount'] < 1) {
    form_set_error('amount', t('The donation amount entered is invalid'));
    return;
  }
  // Generate transaction and place in session. Abort on failure.
  $txn_id = _interswitch_donate_create_txn_id($form_state['values']['amount'],
      $form_state['values']['donation_purpose']);
  if (!$txn_id) {
    // @todo log this error.
    form_set_error('amount',
    t('An unexpected error has occured. Please wait a while and try again.'));
    return;
  }
  $_SESSION['donate_values'] = array(
    'amount' => $form_state['values']['amount'],
    'txn_id' => $txn_id,
    'donation_purpose' => $form_state['values']['donation_purpose']
  );
}

/**
 * Submission callback for _interswitch_donate_donate().
 *
 * See http://drupal.org/node/751826 for Drupal's Form API
 */
function _interswitch_donate_donate_submit($form, &$form_state) {
  $form_state['redirect'] = array(
    'display-details'
  );
}

/**
 * Callback for interswitch-login menu item.
 *
 * It redirects the user to interswitch
 *
 * @return array
 *   Form with JS that redirects the user to interswitch
 */
function _interswitch_donate_goto_interswitch() {
  // If the session contains no donation info, redirect to the donation page.
  if (!isset($_SESSION['donate_values']['amount']) || !isset($_SESSION['donate_values']['txn_id'])) {
    drupal_goto('donate');
  }
  global $user;
  $c = _interswitch_donate_get_config();
  $amount = $_SESSION['donate_values']['amount'] * 100;
  $txn_id = $_SESSION['donate_values']['txn_id'];
  unset ($_SESSION['donate_values']);

  $site_url = url('', array('absolute' => TRUE));
  // @todo: make this configurable.
  $return_url = str_replace('http://www.', 'http://',
      url('interswitch-update-txn', array('absolute' => TRUE)));
  $product_id = $c['product_id'];
  $pay_item_id = $c['pay_item_id'];
  $hash = _interswitch_donate_calculate_hash($txn_id, $product_id,
      $c['mac_key'], $pay_item_id, $amount);

  // If credentials not set.
  if (!$hash || !$product_id || !$pay_item_id) {
    $error = '<p>'
      . t('The Interswitch settings have not been configured. Please goto to the <a href = "@settings-page">settings page</a> to configure them.',
          array('@settings-page' => url('admin/settings/interswitch-donate')))
        . '</p>';
    $form['error'] = array(
      '#value' => $error
    );

    return $form;
  }

  $server = INTERSWITCH_DONATE_DEMO_SERVER;
  if ($c['server'] == 'live') {
    $server = INTERSWITCH_DONATE_LIVE_SERVER;
  }

  $form = array();
  $form['#action'] = $server;
  $form['product_id'] = array(
    '#type' => 'hidden',
    '#value' => $product_id
  );
  $form['pay_item_id'] = array(
    '#type' => 'hidden',
    '#value' => $pay_item_id
  );
  $form['amount'] = array(
    '#type' => 'hidden',
    '#value' => $amount
  );
  $form['currency'] = array(
    '#type' => 'hidden',
    '#value' => '566'
  );
  $form['site_redirect_url'] = array(
    '#type' => 'hidden',
    '#value' => $return_url
  );
  $form['txn_ref'] = array(
    '#type' => 'hidden',
    '#value' => $txn_id
  );
  $form['hash'] = array(
    '#type' => 'hidden',
    '#value' => $hash
  );
  $form['cust_name'] = array(
    '#type' => 'hidden',
    '#value' => $user->name
  );
  $form['site_name'] = array(
    '#type' => 'hidden',
    '#value' => $site_url
  );
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('If you are not redirected in the next 10 seconds, click here')
  );

  drupal_add_js(
    'document.getElementById("-interswitch-donate-goto-interswitch").submit()',
    'inline',
    'footer'
  );

  return $form;
}

/**
 * Calculates hash.
 *
 * Calculates hash for payment ($hash_for_payment = TRUE) or
 * for transaction lookup ($hash_for_payment = FALSE).
 *
 * @param string $txn_id
 *   Transaction reference
 * @param string $product_id
 *   Product ID
 * @param string $mac_key
 *   MAC Key
 * @param string $pay_item_id
 *   Pay item ID
 * @param number $amount
 *   Payment amount
 * @param bool $hash_for_payment
 *   Specifies whether the hash to be calculated is for payment or transaction
 *   lookup
 *
 * @return string
 *   The calculated hash
 */
function _interswitch_donate_calculate_hash($txn_id, $product_id, $mac_key, $pay_item_id = NULL, $amount = NULL, $hash_for_payment = TRUE) {
  if ($hash_for_payment) {
    // $return_url = url('interswitch-update-txn', array('absolute' => TRUE));
    // @todo: make this configurable.
    $return_url = str_replace('http://www.', 'http://',
      url('interswitch-update-txn', array('absolute' => TRUE)));
    return hash('sha512', $txn_id . $product_id . $pay_item_id . $amount
      . $return_url . $mac_key);
  }
  else {
    return hash('sha512', $product_id . $txn_id . $mac_key);
  }
}

/**
 * Callback on user redirection to site.
 *
 * This function is called when the user is redirected back to the site from
 * Interswitch. It updates the transaction information and displays the
 * appropriate text to the user.
 *
 * @return string
 *   Page content in HTML reflecting the outcome of the transaction
 */
function _interswitch_donate_update_txn() {
  unset ($_SESSION['donate_values']);

  $txnid = $_POST['txnref'];
  $pay_ref = $_POST['payRef'];
  // If no response parameters abort and go to the donation form.
  if (!$txnid) {
    watchdog(
      'interswitch_donate',
      'Empty transaction ID for transaction look up',
      array(),
      WATCHDOG_ALERT
    );
    drupal_goto('donate');
  }

  $time = time();
  $r = db_query('SELECT completed, amount, purpose FROM'
      . " {interswitch_donate_donations} WHERE txn_id = '%s'", $txnid);
  $txn_info = db_fetch_object($r);
  if (!$txn_info) {
    watchdog(
      'interswitch_donate',
      "Transaction ID $txnid not found in database for transaction look up. This could be a spoof",
      array(),
      WATCHDOG_ALERT
    );
    drupal_goto('donate');
  }
  // Possible spoof check. Log if this is already a concluded transaction.
  if ($txn_info->completed) {
    // This txn may be a spoof,
    // @todo: notify admin/donation about this.
    watchdog(
      'interswitch_donate',
      "Transaction ID $txnid is already a concluded transaction. This could be a spoof",
      array(),
      WATCHDOG_ALERT
    );
  }

  global $user;
  $c = _interswitch_donate_get_config();
  $base_url = str_replace('www.', '',
      parse_url(url('', array('absolute' => TRUE)), PHP_URL_HOST));

  // Look up txn on ISW.
  $product_id = $c['product_id'];
  $amount = (int) $txn_info->amount * 100;
  $hash = _interswitch_donate_calculate_hash($txnid, $product_id, $c['mac_key'],
    NULL, NULL, FALSE);
  $result = _interswitch_donate_update_txn_from_isw($product_id, $amount,
    $hash, $txnid, $c['server']);

  if ($result->error) {
    // @todo: this should have a support mail link.
    watchdog(
      'interswitch_donate',
      "Error looking up transaction ID $txnid on Interswitch.",
      array(),
      WATCHDOG_ERROR
    );
    drupal_set_message(t('Cannot look up payment information'), 'error');

    return t('There was an error looking up the payment information<br/><br/><br/>Make another <a href="@url">donation</a>',
      array('@url' => url('donate')));
  }
  // Parse the response.
  $json = json_decode($result->data, TRUE);

  $json['Amount'] = $json['Amount'] / 100;
  $amount = $amount / 100;

  // Update the transaction info and update the DB.
  $data['txn_id'] = $txnid;
  $data['status'] = $json['ResponseCode'] == 'Z6' ? 'cancelled' : 'completed';
  $data['completed'] = $time;
  $data['amount_paid'] = $json['Amount'];
  $data['webpay_payref'] = $json['PaymentReference'];
  $data['response_code'] = $json['ResponseCode'];
  $data['response_description'] = $json['ResponseDescription'];
  if (SAVED_UPDATED != drupal_write_record('interswitch_donate_donations', $data, 'txn_id')) {
    // @todo: decide what to do here.
  }

  // Determine what to display to the user.
  if ('00' == $json['ResponseCode'] && $json['Amount'] == $amount) {
    // Success.
    drupal_set_message(t('Payment successful.'), 'status');

    // Notify user.
    drupal_mail(
      'interswitch_donate',
      'success1',
      $user->mail,
      language_default(),
      array(
        'target' => 'user',
        'amount' => $amount,
        'purpose' => $txn_info->purpose,
        'txnid' => $txnid
      ),
      $c['notification_email'],
      TRUE
    );

    // Notify admin.
    drupal_mail(
      'interswitch_donate',
      'success1',
      $c['notification_email'],
      language_default(),
      array(
        'target' => 'admin',
        'amount' => $amount,
        'purpose' => $txn_info->purpose,
        'txnid' => $txnid
      ),
      $c['notification_email'],
      TRUE
    );

    return t("Thank you for your donation.<br/><strong>Transaction ID:</strong> $txnid<br/><strong>WebPay Reference:</strong> $pay_ref<br/><br/><br/>Make another <a href='@url'>donation</a>",
      array('@url' => url('donate')));
  }
  elseif ('00' == $json['ResponseCode'] && $json['Amount'] != $amount) {
    // Success but different amount.
    $_amount = $amount;
    $_amount_paid = $json['Amount'];
    drupal_set_message(
      t("Payment incomplete. Transaction ID: $txnid"),
      'warning'
    );

    // Notify user.
    drupal_mail(
      'interswitch_donate',
      'success2',
      $user->mail,
      language_default(),
      array(
        'target' => 'user',
        'amount' => $amount,
        'amount2' => $json['Amount'],
        'purpose' => $txn_info->purpose,
        'contact' => $c['notification_email'],
        'txnid' => $txnid
      ),
      $c['notification_email'],
      TRUE
    );

    // Notify admin.
    drupal_mail(
      'interswitch_donate',
      'success2',
      $c['notification_email'],
      language_default(),
      array(
        'target' => 'admin',
        'amount' => $amount,
        'amount2' => $json['Amount'],
        'purpose' => $txn_info->purpose,
        'txnid' => $txnid
      ),
      $c['notification_email'],
      TRUE
    );

    return t("Thank you for your donation. However the amount you wished to donate is different from the amount charged to your card.<br/>Details:<br/>Amount for donation: !amount<br/>Amount charged: !amount_paid<br/><strong>Transaction ID:</strong> $txnid<br/><strong>WebPay Reference:</strong> $pay_ref<br/><br/><br/>Make another <a href='@url'>donation</a>",
        array(
          '@url' => url('donate'),
          '!amount' => '&#8358;' . number_format($_amount, 2),
          '!amount_paid' => '&#8358;' . number_format($_amount_paid, 2)
        )
    );
  }
  elseif ('00' != $json['ResponseCode']) {
    // Not successful.
    drupal_set_message(t('Payment not successful'), 'error');

    // Notify user.
    drupal_mail(
      'interswitch_donate',
      'failure',
      $user->mail,
      language_default(),
      array(
        'target' => 'user',
        'amount' => $amount,
        'purpose' => $txn_info->purpose,
        'txnid' => $txnid
      ),
      $c['notification_email'],
      TRUE
    );

    // Notify admin.
    drupal_mail(
      'interswitch_donate',
      'failure',
      $c['notification_email'],
      language_default(),
      array(
        'target' => 'admin',
        'amount' => $amount,
        'purpose' => $txn_info->purpose,
        'txnid' => $txnid
      ),
      $c['notification_email'],
      TRUE
    );

    return t("The payment was not successful.<br/><strong>Transaction ID:</strong> $txnid<br/><strong>WebPay Reference:</strong>$pay_ref<br/><strong>Reason:</strong> @resp<br/><br/><br/>Make another <a href='@url'>donation</a>",
        array(
          '@url' => url('donate'),
          '@resp' => $json['ResponseDescription']
        )
    );
  }
}
/**
 * Gets transaction information from ISW. Done to keep things DRY.
 */
function _interswitch_donate_update_txn_from_isw($product_id, $amount, $hash, $txn_id, $server) {
  if ($server == 'live') {
    $request_url = INTERSWITCH_DONATE_LIVE_LOOKUP_SERVER;
  }
  else {
    $request_url = INTERSWITCH_DONATE_DEMO_LOOKUP_SERVER;
  }

  $request_url .= "?productid=$product_id&transactionreference=$txn_id&amount=$amount";
  $request_headers = array('Hash' => $hash);
  $request_method = 'GET';
  $request_retry = 3;

  return drupal_http_request(
    $request_url,
    $request_headers,
    $request_method,
    '',
    $request_retry
  );
}

/**
 * Callback for display-details menu item.
 *
 * It displays the details of the donation before the user is redirected to
 * Interswitch.
 *
 * @return string
 *   Details of donation with options to cancel or proceed
 */
function _interswitch_donate_display_details() {
  if (!isset($_SESSION['donate_values']['amount']) || !isset($_SESSION['donate_values']['txn_id']) || !isset($_SESSION['donate_values']['donation_purpose'])) {
    drupal_goto('donate');
  }

  return drupal_get_form('_interswitch_donate_donation_details');
}

/**
 * Builds form containing donation details.
 *
 * Called by _interswitch_donate_display_details()
 */
function _interswitch_donate_donation_details($form_state) {
  $form['about'] = array(
   '#type' => 'item',
   '#title' => t('Information'),
   '#value' => t('Please review the details of your donation below')
  );
  $form['amount'] = array(
    '#type' => 'item',
    '#title' => t('Amount (&#8358;)'),
    '#value' => number_format($_SESSION['donate_values']['amount'], 2)
  );
  $form['purpose'] = array(
    '#type' => 'item',
    '#title' => t('Purpose'),
    '#value' => $_SESSION['donate_values']['donation_purpose']
  );
  $form['txnid'] = array(
    '#type' => 'item',
    '#title' => t('Transaction ID'),
    '#value' => $_SESSION['donate_values']['txn_id']
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Donate')
  );
  $form['submit2'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel and go back')
  );

  return $form;
}

/**
 * Submit callback.
 */
function _interswitch_donate_donation_details_submit($form, &$form_state) {
  // If the clicked button is 'Donate' go to interswitch
  // otherwise clear the session and go back to the donation page.
  if ($form_state['clicked_button']['#value'] == 'Donate') {
    $form_state['redirect'] = array('interswitch-login');
  }
  else {
    unset ($_SESSION['donate_values']['amount']);
    unset ($_SESSION['donate_values']['txn_id']);
    unset ($_SESSION['donate_values']['donation_purpose']);
    $form_state['redirect'] = array('donate');
  }
}

/**
 * AJAX callback.
 *
 * This function is used by the donation history interface for admin to lookup
 * a payment's detials from Interswitch.
 */
function _interswitch_donate_requery_isw() {
  if (!arg(4)) {
    // @todo: shouldn't this be logged and an alert to the admin?
    module_invoke_all('exit');
    exit;
  }

  // Get the other details from the DB and query ISW server.
  $c = _interswitch_donate_get_config();

  $r = db_query('SELECT amount FROM {interswitch_donate_donations}'
      . " WHERE txn_id = '%s'", arg(4));
  $rec = db_fetch_object($r);
  if (!$rec) {
    // @todo: log this. A spoof. A transaction that does not exist.
  }
  $amount = $rec->amount * 100;
  $hash = _interswitch_donate_calculate_hash(arg(4), $c['product_id'],
      $c['mac_key'], NULL, NULL, FALSE);
  $result = _interswitch_donate_update_txn_from_isw($c['product_id'], $amount,
      $hash, arg(4), $c['server']);

  if ($result->error) {
    // @todo: log this.
    module_invoke_all('exit');
    exit;
  }

  // Update DB.
  $json = json_decode($result->data, TRUE);
  $json['Amount'] = $json['Amount'] / 100;
  $time = time();

  // uUdate the transaction info and update the DB.
  $data['txn_id'] = arg(4);
  $data['status'] = $json['ResponseCode'] == 'Z6' ? 'cancelled' : 'completed';
  $data['amount_paid'] = $json['Amount'];
  $data['response_code'] = $json['ResponseCode'];
  $data['completed'] = $time;
  $data['response_description'] = $json['ResponseDescription'];
  if (SAVED_UPDATED != drupal_write_record('interswitch_donate_donations', $data, 'txn_id')) {
    // @todo: decide what to do here.
  }

  // Output should be txn_id, status, payment success, response code and
  // description.
  $success = $json['ResponseCode'] == '00' ? 'Yes' : 'No';
  $status = ucwords($data['status']);
  $completed = date('g:i A d-M-Y', $time);
  $output = "{$data['txn_id']}/$status/$completed/$success/{$json['ResponseCode']}/{$json['ResponseDescription']}";

  // It is important not to use return otherwise Drupal will display
  // a full HTML page.
  print $output;
  // Tell all other modules to quit.
  module_invoke_all('exit');
  exit;
}
