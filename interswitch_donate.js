Drupal.behaviors.interswitch_donate = function (context) {
  base_url = $('#interswitch_donate_site_base_url').val().replace('http://www.', 'http://');
  $("input[value=Re-query]").bind("click", function(){
    $.ajax({
      // .parent() will refer to the <input/> itself so to get the <tr> parent
      // we'll have to use .parent().parent()
      url: base_url + '/?q=admin/interswitch-donate/ajax/requery/' + $(this).parent().parent().find('td:eq(0)').text(),
      success: function(data) {
        arr = data.split('/');
        // TR containing the transaction ID.
        selector = 'tr:contains(' + arr[0] + ')';
        tr = $(selector);
        // Status.
        tr.find('td:eq(1)').text(arr[1]);
        // Completion time.
        tr.find('td:eq(4)').text(arr[2]);
        // Success.
        tr.find('td:eq(5)').text(arr[3]);
        // Response code.
        tr.find('td:eq(8)').text(arr[4]);
        // Response description.
        tr.find('td:eq(9)').text(arr[5]);
      },
      error: function (data) {
        alert('An unexpected error has occured. Please try again later');
      },
    });
  });
};
