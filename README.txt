-- SUMMARY --

This module allows site users to make donations using their Interswitch bank
cards. The design is of this module has been successfully checked for all the
mandatory requirements of Interswitch's User Acceptance Test as described in
Interswitch's DIY document

Features
* Specify donation areas/purposes
* Configure the module with your MAC key, Product ID and Pay Item ID
* Set the module to use Interswitch's live or demo server
* Get notifications when a successful or unsuccessful donation attempt is made
(the same goes for the user)
* View/download donation history of all users
* Users can view/download their own donation history
* Manually update a donation's transaction information using Interswitch's web
service

-- INSTALLATION --

* Uncompress the module into your modules folder ie
DRUPAL_ROOT/sites/all/modules
* Enable the module from the modules pages ie yoursite.com/admin/build/modules

-- CONFIGURATION --

* Enter your Merchant credentials, notification email and donation areas on the
setting page at yoursite.com/admin/settings/interswitch-donate/settings
* View all donation history for all users at
yoursite.com/admin/settings/interswitch-donate/view-all-history
* Create menu links for your users to make donations and view their individual
donation histories:
  - the donation link should point to yoursite.com/donate
  - the donation history link for the individual user should point to
  yoursite.com/view-donations 

-- TROUBLESHOOTING --

* If a user gets an "Access denied" page when trying to make a donation or view
donation history:
  - make sure the 'access donation form' permission has been granted to
  authenticated user at yoursite.com/admin/user/permissions 

-- CREDITS --

This project was made possible and facilitated by the kind contributions of the
Grailland IT Group:
Adewale Carpenter
Ayodeji Agboola
Emeka Mbaneme
Emmanuel Eze
McNeil Afegbah
Ogonna Chinwike
Ruke Awaritefe
UgoChukwu Onukwu
Uzo Eziukwu

Thanks guys.
